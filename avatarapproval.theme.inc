<?php

/**
 * @file
 * Theme callbacks for Avatar approval.
 */

function theme_avatarapproval_form($form) {
  // Place the form options at the top of the page
  $output = drupal_render($form['options']);
  // If a there are avatars in this form
  if (isset($form['picture']) && is_array($form['picture'])) {
    // For each avatar, we are going to build a table row
    foreach(element_children($form['picture']) as $key) {
      $row = array();;
      $row[] = drupal_render($form['avatars'][$key]);
      $row[] = drupal_render($form['picture'][$key]);
      $row[] = drupal_render($form['users'][$key]);
      $row[] = drupal_render($form['timestamp'][$key]);
      $row[] = drupal_render($form['moderator'][$key]);
      // Then we will set each row into an array of rows
      $rows[] = $row;
    }
  }
  else {
    // If there were no avatars to display
    $rows[] = array(array('data' => t('No avatars available.'), 'colspan' => 5));
  }
  // Send the array to the table theme function
  $output .= theme('table', $form['header']['#value'], $rows);

  // Add the pager feature
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);
  return $output;
}

function theme_avatarapproval_image($image, $approval) {
  return theme('image', $image, "avatar", "avatar", array( 'class' => 'avatar_approval'), FALSE);
}

function theme_avatarapproval_profile($avatars) {
  if ($avatars[AVATAR_DISAPPROVED]) {
    $avatars[AVATAR_DISAPPROVED]['title'] = 'Not approved';
  }
  if ($avatars[AVATAR_NOT_YET_MODERATED]) {
    $avatars[AVATAR_NOT_YET_MODERATED]['title'] = 'Pending approval';
  }
  return $avatars;
}

function theme_avatarapproval_get_name($users = NULL) {
  $output = '';
  if ($users) {
    foreach($users as $uid => $name) {
      $output .= l($name, 'user/'. $uid) . '<br />';
    }
  }
  else {
    $output = t('None');
  }

  return $output;
}

/**
 * Return a themed list of all the usernames associated
 * to a given md5
 */
function avatarapproval_get_users($md5) {
  $sql = 'SELECT u.name, u.uid FROM {avatar_approval} a '.
    'INNER JOIN {users} u ON u.uid = a.uid '.
    'WHERE a.md5 = "%s" AND a.uid != %d AND active = %d';
  $user_results = db_query($sql, $md5, 0, AVATAR_ACTIVE);
  $results = array();
  while($user = db_fetch_object($user_results)) {
    $results[$user->uid] = $user->name;
  }

  return theme('avatarapproval_get_name', $results);
}