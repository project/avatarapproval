<?php

/**
 * @file
 * Page callbacks for Avatar approval.
 */

/**
 * This is were we create the approval menu items and callbacks
 */
function _avatarapproval_operations() {
  $operations = array(
    'approve' => array(
      'text' => t('Approve the selected avatars'),
      'callback' => 'avatarapproval_do_approval'),
    'disapprove' => array(
      'text' => t('Disapprove the selected avatars'),
      'callback' => 'avatarapproval_do_disapprove'),
  );
  // We only want to give deletion power to priviliged people
  if (user_access('delete avatars')) {
    $operations['delete'] = array(
      'text' => t('Delete the selected avatars'),
      'callback' => 'avatarapproval_do_delete');
  }
  return $operations;
}

/**
 * Return a themed list of usernames associated to a given md5
 */
function _avatarapproval_get_users($md5) {
  $sql = 'SELECT u.name, u.uid FROM {avatar_approval} a '.
    'INNER JOIN {users} u ON u.uid = a.uid '.
    'WHERE a.md5 = "%s" AND a.uid != %d AND active = %d';
  $user_results = db_query($sql, $md5, 0, AVATAR_ACTIVE);
  $results = array();
  while($user = db_fetch_object($user_results)) {
    $results[$user->uid] = $user->name;
  }

  return theme('avatarapproval_get_name', $results);
}
/**
 * This function displays the avatar approval administration page.
 * It passes url arg so the form can decide what data it should display
 */
function avatarapproval_admin_overview($arg = "") {
  // If user pictures are not enabled
  if (!variable_get('user_pictures', 0)) {
    if (user_access('access administration pages')) {
      $message = t('User pictures are not !enabled.',
      array( '!enabled' => l(t('enabled'), 'admin/user/settings'))
      );
    }
    else {
      $message = t('User pictures are not enabled.');
    }
    drupal_set_message($message);
  }

  return drupal_get_form('avatarapproval_admin_overview_form', $arg);
}

/**
 * The avatar approval admin form
 */
function avatarapproval_admin_settings_form() {
  $form = array();
  // Define fieldsets
  $form['awaitingpic_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Awaiting approval avatar settings'),
    '#collapsible' => true
  );
  $form['notifications_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Reminders settings'),
    '#collapsible' => true
  );

  $form['notifications_fieldset']['notifications_email_fieldset'] = array(
    '#type' => 'fieldset',
    '#weight' => 1,
    '#title' => t('Email notification settings')
  );
  // Settings interface
  $form['awaitingpic_fieldset']['avatarapproval_use_awaiting_picture'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use a specific picture for users awaiting approval avatar'),
    '#description' => t('Choose either to use a specific picture for the avatar of the user who have submitted a picture for approval or to use the default avatar picture.'),
    '#default_value' => variable_get('avatarapproval_use_awaiting_picture', true),
  );
  $form['awaitingpic_fieldset']['avatarapproval_awaiting_picture_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Awaiting approval avatar picture'),
    '#description' => t('Path of the avatar to use for the users who have submitted a picture for approval.'),
    '#default_value' => variable_get('avatarapproval_awaiting_picture_url', AVATAR_AWAITING_APPROVAL),
  );

  $form['notifications_fieldset']['avatarapproval_notify_moderator_login'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify pending avatar approvals on moderator login'),
    '#description' => t('If an user has the permission to approve avatars, notify that there are pending requests when he logs in.'),
    '#default_value' => variable_get('avatarapproval_notify_moderator_login', true),
  );

  $form['notifications_fieldset']['avatarapproval_notify_moderator_email'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify pending avatar approvals to moderators by email'),
    '#description' => t('If an user has the permission to approve avatars, notify that there are pending requests by sending a periodical email.'.
                       'The period is variable and depends on a threshold. If the threshold is 3, the mail is sent when queue length is 3, 9, 27...'),
    '#default_value' => (variable_get('avatarapproval_notify_moderator_email', true)),
  );
  $mail_token_help = t('Available variables are:') .implode(", ", array_keys(avatarapproval_mail_tokens()));
  $form['notifications_fieldset']['notifications_email_fieldset']['avatarapproval_notify_moderator_email_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#description' => $mail_token_help,
    '#default_value' => _avatarapproval_mail_text('avatarapproval_notify_moderator_email_subject'),
    '#maxlength' => 180,
  );
  $form['notifications_fieldset']['notifications_email_fieldset']['avatarapproval_notify_moderator_email_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#description' => $mail_token_help,
    '#default_value' => _avatarapproval_mail_text('avatarapproval_notify_moderator_email_body')
  );
  $form['notifications_fieldset']['notifications_email_fieldset']['avatarapproval_notify_moderator_email_threshold'] = array(
    '#type' => 'textfield',
    '#title' => t('Approval queue length threshold'),
    '#description' => t('Please specify how many requests must be queued prior to sending notification e-mails. If you set the value to 1, an e-mail is sent every new request.'),
    '#default_value' => variable_get('avatarapproval_notify_moderator_email_threshold', 3),
    '#maxlength' => 5,
    '#size' => 5
  );

  $form['#submit'][] = "avatarapproval_admin_settings_form_submit";
  $form['#validate'][] = "avatarapproval_admin_settings_form_validate";

  return system_settings_form($form);
}

function avatarapproval_admin_settings_form_submit(&$form, &$form_state) {
  // Change approval picture
  $oldpic = variable_get('avatarapproval_awaiting_picture_url', AVATAR_AWAITING_APPROVAL);
  $newpic = $form['#post']['avatarapproval_awaiting_picture_url'];
  if ($form['#post']['op'] == t("Reset to defaults")) {
    variable_set('avatarapproval_awaiting_picture_url', AVATAR_AWAITING_APPROVAL);
    $newpic = AVATAR_AWAITING_APPROVAL;
  }

  if ($newpic != $oldpic) {
    avatarapproval_refresh_awaiting_avatar($newpic, $oldpic, true);
  }
}

function avatarapproval_admin_settings_form_validate(&$form, &$form_state) {

  // None of the following actions must be taken for a reset setting operation,
  // so we return
  if ($form['#post']['op'] == t("Reset to defaults")) {
    return;
  }

  if (!is_numeric($form['#post']['avatarapproval_notify_moderator_email_threshold']) ||
      $form['#post']['avatarapproval_notify_moderator_email_threshold'] <= 0) {
    form_set_error('avatarapproval_notify_moderator_email_threshold', t("Threshold value must be a positive integer number."));
  }

  $newpic = $form['#post']['avatarapproval_awaiting_picture_url'];
  if (!file_exists($newpic)) {
    form_set_error('avatarapproval_awaiting_picture_url', t("The selected file cannot be found, or there are insufficent permissions to access it."));
  }
}

/**
 * The avatar approval form builder
 */
function avatarapproval_admin_overview_form($form_state, $type) {
  // The beginings of the Update Options form
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );
  // Get the operations defined earlier in the _avatarapproval_operations function
  $operations = _avatarapproval_operations();
  // Unset the operation for the current screen since the avatar already has that state
  unset($operations[$type]);
  $options = array();
  foreach ($operations as $key => $value) {
    // Set each option
    $options[$key] = $value['text'];
  }

  // We create the options form
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => 'approve'
  );
  $form['options']['submit'] = array(
  '#type' => 'submit',
  '#value' => t('Update')
  );

  // Next we load the forms header
  $form['header'] = array(
    '#type' => 'value',
    '#value' => array(
      array(
        'data' => '<input type="checkbox" onclick = "$(\'#avatarapproval-admin-overview-form .form-checkbox\').attr(\'checked\', $(this).attr(\'checked\'));" />',
        'field' => null
      ),
      array(
        'data' => t('Avatar'),
        'field' => 'avatar'
      ),
      array(
        'data' => t('User'),
        'field' => 'timestamp',
      ),
      array(
        'data' => t('Time'),
        'field' => 'timestamp',
        'sort' => 'desc'
       ),
      array(
        'data' => t('Moderated by'),
        'field' => 'moderator',
        'sort' => 'desc'
      ),
    )
  );

  // Set the basic SQL Statement
  $sql = 'SELECT md5, timestamp, moderator, extension FROM {avatar_approval} WHERE ';

  // If $type is NULL, they are at the AVATAR_APPROVAL_PAGE.  The
  // default menu setting shows all avatars that need to be moderated.
  if ($type == "") {
    $sql .= 'approved = '. AVATAR_NOT_YET_MODERATED;
  }
  else if ($type == 'disapprove') {
    $sql .= 'approved = '. AVATAR_DISAPPROVED;
  }
  else if ($type == 'approve') {
    $sql .= 'approved = '. AVATAR_APPROVED;
  }

  $picture_directory = _avatarapproval_get_directory();

  $result = pager_query($sql . tablesort_sql($form['header']['#value']), 25, 0, NULL);
  $avatars = array();

  while ($avatar = db_fetch_object($result)) {
    if (!$avatar->moderator){
      $avatar->moderator = t('Pending moderation');
    }
    $avatars[$avatar->md5] = '';
    // [iva2k] fixing #716402:
    $form['picture'][$avatar->md5] = array(
      '#value' => theme('image', file_create_url($picture_directory . $avatar->md5 . '.' . $avatar->extension), '', '', '', FALSE)
    );
    $form['users'][$avatar->md5] = array('#value' => _avatarapproval_get_users($avatar->md5));
    $form['timestamp'][$avatar->md5] = array('#value' => format_date($avatar->timestamp, 'small'));
    $form['moderator'][$avatar->md5] = array('#value' => $avatar->moderator);
  }

  $form['avatars'] = array('#type' => 'checkboxes', '#options' => $avatars);
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
  $form['#theme'] = 'avatarapproval_form';
  return $form;
}

/**
 * The validation handler for the avatar approval admin form
 */
function avatarapproval_admin_overview_form_validate($form, $form_state) {
  // If no boxes were checked, set an error
  if (count($form['#post']['avatars']) == 0) {
    form_set_error('', t('Please select one or more avatars to perform the update on.'));
  }
}

/**
 * The submit function for the avatar approval admin form.
 */
function avatarapproval_admin_overview_form_submit($form, $form_state) {
  $edit = $form['#post'];
  // Get a list of operations
  $operations = _avatarapproval_operations();
  // Select the proper callback from for the operation
  // ie: $operation['disapprove']['callback']
  if  ($operations[$edit['operation']]['callback']) {
    $callback = $operations[$edit['operation']]['callback'];
    // Then run through all the avatars that were checked'
    foreach($edit['avatars'] as $md5 => $value) {
      if  (!is_null($callback)) {
        $callback($md5);
      }
    }
  }
  cache_clear_all();
  drupal_set_message(t('The update ('. check_plain($edit['operation']). ') has been performed.'));
}