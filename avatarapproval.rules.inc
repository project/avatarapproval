<?php

/**
 * @file
 * Rules integration for the Avatar approval module.
 */

/**
 * Implementation of hook_rules_event_info().
 */
function avatarapproval_rules_event_info() {
  return array(
    'avatarapproval_avatar_add' => array(
      'label' => t('A new avatar is added to moderation queue'),
      'arguments' => avatarapproval_rules_events_hook_arguments(),
      'module' => 'Avatar approval',
    ),
    'avatarapproval_avatar_approve' => array(
      'label' => t('An avatar is approved'),
      'arguments' => avatarapproval_rules_events_hook_arguments(),
      'module' => 'Avatar approval',
    ),
    'avatarapproval_avatar_disapprove' => array(
      'label' => t('An avatar is disapproved'),
      'arguments' => avatarapproval_rules_events_hook_arguments(),
      'module' => 'Avatar approval',
    ),
 );
}

/**
 * Describes the arguments available for the avatarapproval_hook().
 *
 */
function avatarapproval_rules_events_hook_arguments() {
  return array(
    'uid' => array(
      'type' => 'number',
      'hidden' => TRUE,
    ),
    'mod_uid' => array(
      'type' => 'number',
      'hidden' => TRUE,
    ),
    'mod_op_id' => array(
      'type' => 'number',
      'hidden' => TRUE,
    ),
    'user' => array(
      'type' => 'user',
      'label' => t('User that had his avatar moderated'),
      'handler' => 'avatarapproval_rules_events_argument_user',
    ),
    'moderator' => array(
      'type' => 'user',
      'label' => t('Moderator user who performed the operation'),
      'handler' => 'avatarapproval_rules_events_argument_moderator',
    ),
    'operation' => array(
      'type' => 'string',
      'label' => t('Operation performed on the avatar'),
      'handler' => 'avatarapproval_rules_events_argument_operation',
    ),
  ) + rules_events_global_user_argument();
}

/**
 * Handler to get the user.
 */
function avatarapproval_rules_events_argument_user($uid, $mod_uid, $mod_op_id) {
  return user_load(array('uid' => $uid));
}

/**
 * Handler to get the moderator.
 */
function avatarapproval_rules_events_argument_moderator($uid, $mod_uid, $mod_op_id) {
	return user_load(array('uid' => $mod_uid));
}

/**
 * Handler to get the operation.
 */
function avatarapproval_rules_events_argument_operation($uid, $mod_uid, $mod_op_id) {
	return avatarapproval_get_operation_name($mod_op_id);
}

/**
 * Implementation of hook_rules_action_info().
 */
function avatarapproval_rules_action_info() {
  return array(
    'avatarapproval_rules_action_avatar_approve' => array(
      'label' => t('Approve avatar'),
      'arguments' => array(
        'user' => array(
          'type' => 'user',
          'label' => t('Owner of the avatar to approve'),
        ),
      ),
      'module' => 'Avatar approval',
    ),
    'avatarapproval_rules_action_avatar_disapprove' => array(
      'label' => t('Disapprove avatar'),
      'arguments' => array(
        'user' => array(
          'type' => 'user',
          'label' => t('Owner of the avatar to disapprove'),
        ),
      ),
      'module' => 'Avatar approval',
    ),
    'avatarapproval_rules_action_avatar_delete' => array(
      'label' => t('Delete avatar'),
      'arguments' => array(
        'user' => array(
          'type' => 'user',
          'label' => t('Owner of the avatar to delete'),
        ),
      ),
      'module' => 'Avatar approval',
    ),
  );
}

/**
 * Action: Approve avatar.
 */
function avatarapproval_rules_action_avatar_approve($user) {
   $md5 = db_result(db_query('SELECT md5 FROM {avatar_approval} WHERE uid = %d AND active = 1 AND approved = -1', $user->uid));
   avatarapproval_do_approval($md5);
}

/**
 * Action: Disapprove avatar.
 */
function avatarapproval_rules_action_avatar_disapprove($user) {
   $md5 = db_result(db_query('SELECT md5 FROM {avatar_approval} WHERE uid = %d AND active = 1 AND approved = -1', $user->uid));
   avatarapproval_do_disapprove($md5);
}

/**
 * Action: Delete avatar.
 */
function avatarapproval_rules_action_avatar_delete($user) {
   $md5 = db_result(db_query('SELECT md5 FROM {avatar_approval} WHERE uid = %d AND approved = -1', $user->uid));
   avatarapproval_do_delete($md5);
}